# Translation of Odoo Server.
# This file contains the translation of the following modules:
#	* report_terms
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 8.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-12-24 10:08+0000\n"
"PO-Revision-Date: 2014-12-24 10:08+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: report_terms
#: model:ir.model,name:report_terms.model_res_company
msgid "Companies"
msgstr ""

#. module: report_terms
#: field:res.company,terms:0
msgid "Terms and Conditions"
msgstr ""

#. module: report_terms
#: view:res.company:report_terms.terms_company
msgid "Terms and conditions"
msgstr ""

