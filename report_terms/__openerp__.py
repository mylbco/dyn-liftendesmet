# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) Bubbles-iT (<http://www.bubbles-it.be>)
#    Copyright (C) 2004 OpenERP SA (<http://www.openerp.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################

{
    'name': 'Terms and Conditions',
    'version': '0.1',
    'category': 'Finance',
    'description': """
    This module will add terms field to the company and print the terms and conditions on the invoice report

Version 0.1
===========
* Add terms and conditions on the invoice document
    """,
    'author': 'Bubbles-iT',
    "website": "http://www.bubbles-it.be",
    'depends': ['account'],
    'init': [],
    'data': [
         'views/report_invoice_terms.xml',
         'company.xml'
    ],
    'demo': [],
    'test': [],
    'installable': True,
    'active': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
