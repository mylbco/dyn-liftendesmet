from openerp import api, models, fields, _
import openerp.addons.decimal_precision as dp


class AccountAnalyticInvoiceIndex(models.Model):
    _name = 'account.analytic.invoice.index'

    name = fields.Char()
    history_ids = fields.One2many('account.analytic.invoice.index.detail', 'index_id',
        string='History')
    current_value = fields.Float(compute='_get_current_value')

    @api.one
    def _get_current_value(self):
        self.current_value = self.history_ids[0].value


class AccountAnalyticInvoiceIndexDetail(models.Model):
    _name = 'account.analytic.invoice.index.detail'
    _order = 'date desc'
    _rec_name = 'index_id'

    @api.one
    def name_get(self):
        return (self.id, "%s - %s" % (self.index_id.name, self.date))

    index_id = fields.Many2one('account.analytic.invoice.index', string='Index',
        required=True)
    date = fields.Date(default=fields.Date.today)
    value = fields.Float()


class AccountAnalyticInvoiceLine(models.Model):
    _inherit = 'account.analytic.invoice.line'

    initial_index_id = fields.Many2one('account.analytic.invoice.index.detail')
    price_unit = fields.Float(compute='_get_indexed', digits_compute=dp.get_precision('Account'))
    price_unit_src = fields.Float(required=True, digits_compute=dp.get_precision('Account'))

    @api.one
    @api.depends('price_unit_src', 'initial_index_id', 'product_id')
    def _get_indexed(self):
        self.price_unit = self.price_unit_src * (self.initial_index_id.index_id.current_value / self.initial_index_id.value)

    def product_id_change(self, cr, uid, ids, product, uom_id, qty=0, name='', partner_id=False, price_unit=False, pricelist_id=False, company_id=None, context=None):
        res = super(AccountAnalyticInvoiceLine, self).product_id_change(
            cr, uid, ids, product, uom_id, qty, name, partner_id, price_unit,
            pricelist_id, company_id, context)
        if 'price_unit' in res.get('value', {}):
            res['value']['price_unit_src'] = res['value']['price_unit']
            del res['value']['price_unit']
        return res


class AccountAnalyticAccount(models.Model):
    _inherit = 'account.analytic.account'

    # TODO Implement changing the price_unit
    # @api.model
    # def _prepare_invoice_lines(self, contract, fiscal_position_id):
    #     res = super(AccountAnalyticAccount, self)._prepare_invoice_lines(contract, fiscal_position_id)
    #     for line in res:
    #         pass
    #     return res
