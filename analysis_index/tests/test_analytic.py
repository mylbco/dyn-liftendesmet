from openerp.tests import TransactionCase


class test_base_invoice_index(TransactionCase):
    def test_10_model(self):
        model = self.env['account.analytic.invoice.index']
        fields = model.fields_get().keys()
        self.assertIn('name', fields)
        self.assertIn('history_ids', fields)

    def test_20_view(self):
        model = self.env['account.analytic.invoice.index']
        view = model.fields_view_get(False, 'form')
        fields = view['fields'].keys()
        self.assertIn('name', fields)
        self.assertIn('history_ids', fields)

class test_base_invoice_line(TransactionCase):
    def test_10_model(self):
        model = self.env['account.analytic.invoice.line']
        fields = model.fields_get().keys()
        self.assertIn('initial_index_id', fields)
        self.assertIn('price_unit_src', fields)


class test_function(TransactionCase):
    def test_10_index(self):
        model = self.env['account.analytic.invoice.index']
        sub_model = self.env['account.analytic.invoice.index.detail']

        index = model.create({'name': 'Test'})
        sub1 = sub_model.create({
            'index_id': index.id,
            'value': 100,
            'date': '2000-01-01',
        })

        sub2 = sub_model.create({
            'index_id': index.id,
            'value': 150,
            'date': '2012-01-01',
        })

        self.assertEqual(index.current_value, 150)
