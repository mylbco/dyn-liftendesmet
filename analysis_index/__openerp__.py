{
    'name': 'Analytic Index Invoicing',
    'version': '0.1',
    'category': 'Sales',
    'summary': 'Indexing module for generating recurring invoices',
    'description': """

History
=======

Version 0.1
-----------
Initial commit
""",
    'images': [],
    'author': 'Bubbles-IT',
    'maintainer': 'Bubbles-IT',
    'website': 'http://www.bubbles-it.be',
    'depends': [
        'account_analytic_analysis',
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/analytic_view.xml',
    ],
    'demo': [
        'analysis_index_demo.xml',
    ],
    'css': [],
    'js': [],
    'qweb': [],
    'test': [],
    'installable': True,
    'application': False,
    'auto_install': False,
}
