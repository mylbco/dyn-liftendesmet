# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) Bubbles-iT (<http://www.bubbles-it.be>)
#    OpenERP, Open Source Management Solution
#    Copyright (C) Bubbles-iT (<http://www.bubbles-it.be>)
#    Copyright (C) 2004 OpenERP SA (<http://www.openerp.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################

{
    'name': 'Invoice report with new header',
    'version': '0.1',
    'category': 'Finance',
    'description': """
    This module will enhance the layout of the invoice report.

Version 0.1
===========
* Changed header - footer   
    """,
    'author': 'Bubbles-iT',
    "website": "http://www.bubbles-it.be",
    'depends': ['account'],
    'init': [],
    'data': [
         'views/company_documents.xml',
         'views/report_invoice.xml',
    ],
    'demo': [],
    'test': [],
    'installable': True,
    'active': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
