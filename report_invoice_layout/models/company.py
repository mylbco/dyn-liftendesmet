from openerp import models, fields, api

class ResCompany(models.Model):
    _inherit = 'res.company'

    do_header = fields.Boolean(string="Print header", default=True)
    do_footer = fields.Boolean(string="Print footer", default=True)
    backgroundcolor = fields.Char(string="Backgroundcolor", help="css color value")
    backgroundimage = fields.Binary("Image", help="This field holds the image, limited to 1024x1024px")
